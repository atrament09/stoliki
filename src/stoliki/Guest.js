import React from "react";
import {Button, ButtonGroup} from '@material-ui/core';

function Guest({ guest, onDelete: handleDelete, onAddGuestToTable: handleAddGuestTabNrBtnClick }) {
    return (
        <div>
            <li>{guest}</li>
            <ButtonGroup size="small" variant="contained" color="secondary" aria-label="contained primary button group">
            <Button onClick={handleAddGuestTabNrBtnClick}>Przypisz</Button>
            <Button >Edit</Button>
            <Button onClick={handleDelete}>Usuń</Button>
            </ButtonGroup>
        </div>
    )
}

export default Guest