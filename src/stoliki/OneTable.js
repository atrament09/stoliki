import React from "react";
import { Button, ButtonGroup } from '@material-ui/core';
import classnames from 'classnames';

function OneTable({
    tabNr, guestlist, isActive,
    onGuestRemoveFromTable: handleRemoveFromTable,
    onTableDelete: handleTableDeleteBtnClick,
    onTableActive: handleTableActiveBtnClick
}) {

    const TableGuests = guestlist
        .filter(f => f.tableNumber === tabNr)
        .map(m => <li key={m.id}> {m.value} {m.tableNumber}
            <Button size="small" variant="outlined" onClick={handleRemoveFromTable(m)}> usuń </Button></li>)

    return (
        <div className={classnames('onetable', { 'onetable-active': isActive })}>
            <div className="table-title">Stolik {tabNr} {isActive && 'wybrany'}</div>
            <ButtonGroup style={{ textAlign: "center" }} size="small" color="secondary" aria-label="outlined secondary button group">
                <Button variant="outlined" onClick={handleTableActiveBtnClick}>Wybierz</Button>
                <Button variant="outlined">Edit</Button>
                <Button variant="outlined" onClick={handleTableDeleteBtnClick}>Usuń</Button>
            </ButtonGroup>
            <br />
            <br />
            {TableGuests}
        </div>
    )
}

export default OneTable;

/**
 * 1. zrob komponent personList
 * 2. przyjmuje guestList
 */