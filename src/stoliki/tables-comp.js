import React, { useState, useEffect } from "react";
import "./tables.css";
import Guest from "./Guest";
import uuid from "uuid";
import axios from 'axios';
import OneTable from "./OneTable";
import {
    Button, Grid, Card, CardContent,
    ListItem, ListItemText, ListItemSecondaryAction,
    List,


} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import CommentIcon from '@material-ui/icons/Comment';
import DeleteIcon from '@material-ui/icons/Delete';

const GUEST_URL = 'http://localhost:3004/guest';
const TABLE_URL = 'http://localhost:3004/stolik';

const GuestList = ({ guestlist, children }) => {
    return (
        <List>
            {guestlist && guestlist.map(guest => {
                return (
                    <ListItem key={guest.id} role={undefined} dense>
                        <ListItemText primary={guest.value} />
                        <ListItemSecondaryAction>
                            {children(guest)}
                        </ListItemSecondaryAction>
                    </ListItem>
                );
            })}
        </List>
    )
}

function Stoliki() {
    const [guestlist, setGuestlist] = useState([]);
    const [tablelist, setTablelist] = useState([]);
    const [activeTable, setActiveTable] = useState("");
    const [guest, setGuest] = useState("");
    const [table, setTable] = useState("");

    const [isUpdated, setIsUpdated] = useState(false);

    const handleGuestInput = (event) => {
        const { value } = event.target;
        setGuest(value);
    }

    const handleAddGuestBtnClick = () => {
        const newItem = { value: guest, id: uuid(), tableNumber: null };
        axios.post(GUEST_URL, newItem)
            .then(() => {
                setGuestlist([...guestlist, newItem]);
                setGuest("");
            })
            .catch((error) => {
                alert('ERROR!');
                console.error(error);
            })
    }

    const handleGuestDelete = (item) => () => {
        axios.delete(`${GUEST_URL}/${item.id}`)
            .then(() => {
                setGuestlist([...guestlist.filter(r => r !== item)])
            })
            .catch((error) => {
                alert('ERROR!');
                console.error(error);
            })
    }
    const handleTableInputChange = (event) => {
        const { value } = event.target;
        setTable(value);
    }

    const handleTableAdd = () => {
        const newTable = { name: table, id: uuid() };
        axios.post(TABLE_URL, newTable)
            .then(() => {
                setTablelist([...tablelist, newTable]);
                setTable("");
            })
            .catch((error) => {
                alert('ERROR!');
                console.error(error);
            })
    }

    const handleTableDeleteBtnClick = (item) => () => {
        axios.delete(`${TABLE_URL}/${item.id}`)
            .then(() => {
                setTablelist([...tablelist.filter((t) => t !== item)])
            })
            .catch((error) => {
                alert('ERROR!');
                console.error(error);
            })
    }

    const handleTableActiveBtnClick = (item) => () => {
        setActiveTable(item.name)
        console.log(item.name)
    }

    const assignTableToGuest = (item, table) => {
        axios.patch(`${GUEST_URL}/${item.id}`,
            { tableNumber: table })
            .then(() => {
                setGuestlist([...guestlist
                    .map(guest => {
                        if (guest.id === item.id) {
                            guest.tableNumber = table;
                        }
                        return guest;
                    })]);
            })
            .catch((error) => {
                alert('ERROR!');
                console.error(error);
            })
    }

    const handleAddGuestTabNrBtnClick = (item) => () => {
        assignTableToGuest(item, activeTable);
    }

    const handleGuestRemoveFromTable = (item) => () => {
        assignTableToGuest(item, null);
    }


    useEffect(() => {
        if (!isUpdated) {
            axios.get(GUEST_URL)
                .then((response) => {
                    setIsUpdated(true);
                    setGuestlist(response.data)
                })
                .catch(console.error)
        }
    }, [isUpdated, guestlist])

    useEffect(() => {
        if (!isUpdated) {
            axios.get(TABLE_URL)
                .then((res) => {
                    setIsUpdated(true);
                    setTablelist(res.data)
                })
                .catch(console.error)
        }
    }, [isUpdated])

    return (
        <div>
            <h1 className="title">Wesele Kulów - 15 sierpnia 2020 r.</h1>
            <Grid container style={{ backgroundColor: "#ab7a7a" }}>
                <Grid item xs={6}>
                    <div className="guest-list">
                        <h2 className="listsTitle">Lista gości</h2>
                        <input
                            placeholder="Wpisz gościa"
                            value={guest}
                            onChange={handleGuestInput} />
                        <br />
                        <Button
                            onClick={handleAddGuestBtnClick}
                            disabled={!guest}
                            variant="contained"
                        >Dodaj do listy gości</Button>
                        <div>
                            <ol>
                                {guestlist
                                    .filter(f => f.tableNumber === null)
                                    .map(g =>
                                        <Guest
                                            key={g.id}
                                            guest={g.value}
                                            onDelete={handleGuestDelete(g)}
                                            onAddGuestToTable={handleAddGuestTabNrBtnClick(g)}
                                        />
                                    )}
                            </ol>
                        </div>
                        <GuestList guestlist={guestlist}>
                            {
                                (guest) => (
                                    <>
                                        <IconButton onClick={handleGuestDelete(guest)} edge="end" aria-label="comments">
                                            <DeleteIcon />
                                        </IconButton>
                                        <IconButton edge="end" aria-label="comments">
                                            <CommentIcon />
                                        </IconButton>
                                    </>
                                )
                            }

                        </GuestList>
                    </div>
                </Grid>
                <Grid item xs={6}>
                    <div className="table-list">
                        <h2 className="listsTitle">Lista stolików</h2>
                        <input
                            placeholder="Wpisz numer stolika"
                            value={table}
                            onChange={handleTableInputChange} />
                        <br />
                        <Button
                            onClick={handleTableAdd}
                            disabled={!table}
                            variant="contained">Dodaj stolik</Button>

                        {tablelist.map(t => (
                            <Card key={t.id} variant="outlined">
                                <CardContent>
                                    <OneTable
                                        key={t.id}
                                        tabNr={t.name}
                                        onGuestRemoveFromTable={handleGuestRemoveFromTable}
                                        onTableDelete={handleTableDeleteBtnClick(t)}
                                        onTableActive={handleTableActiveBtnClick(t)}
                                        guestlist={guestlist}
                                        isActive={activeTable === t.name}
                                    />
                                </CardContent>
                            </Card>
                        ))}
                    </div>
                </Grid>
            </Grid>
        </div>
    )
}

export default Stoliki;

// wlasny use hook https://pl.reactjs.org/docs/hooks-custom.html
