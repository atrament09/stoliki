import React from 'react';
import './App.css';
import Stoliki from './stoliki/tables-comp';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';

function App() {
  return (
    <div className="main">
      <CssBaseline />
      <Container maxWidth="lg">
        <Stoliki />
      </Container>
    </div>
  );
}

export default App;
